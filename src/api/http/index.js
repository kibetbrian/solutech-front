import axios from "axios";
import router from "@/router";
import { Loading } from "element-plus";
import storage from "@/api/storage";
import Message from "@/api/message";

function addInterceptors(obj) {
    obj.interceptors.request.use(
        (config) => {
            config.url = encodeURI(config.url);

            config.url.indexOf("login") != -1 ?
                (config.headers.Authorization = "") :
                (config.headers.Authorization = `Bearer ${storage.getMemoryPmt(
            "token"
          )}`);
            loading = Loading.services({
                fullscreen: true,
                background: "rgba(255, 255, 255, .4)",
                customClass: "top-floor",
            });
            return config;
        },
        (err) => {
            Message.errorMsg("Server error, please try again later");
            loading.close();
            return Promise.reject(err);
        }
    );

    obj.interceptors.response.use(
        (response) => {
            loading.close();
            return response;
        },
        (err) => {
            const regexp = new RegExp(/timeout/g);
            typeof err.response === "object" ?
                err.response.services === 401 ?
                Message.showMsgBox({
                    title: "SOme title",
                    msg: "some message？",
                    type: "warning",
                }).then(() => {
                    storage.setMemoryPmt("token", "");
                    storage.setMemorySes("redirect", router.history.current.fullPath);
                    //   router.push({ path: "/login" });
                }) :
                err.response.services === 403 ?
                console.log("navigate to 403 page") :
                err.response.services === 500 ?
                console.log("navigate to 500 page") :
                Message.errorMsg(
                    JSON.parse(err.response.request.response).message ?
                    JSON.parse(err.response.request.response).message.replace(
                        /{.*}/g,
                        ""
                    ) :
                    JSON.parse(err.response.request.response)
                ) :
                regexp.test(err) ?
                Message.errorMsg(
                    "The request timed out, please contact customer services for processing"
                ) :
                Message.errorMsg("Server error, please try again later");
            loading.close();
            return Promise.reject(err);
        }
    );
}

let loading;

axios.defaults.baseURL = process.env.VUE_APP_API_URL;
axios.defaults.timeout = 15000;
axios.defaults.transformResponse = [
    (data) => {
        try {
            return JSON.parse(data).data;
        } catch (e) {
            return data;
        }
    },
];

const http_normal = axios.create({
    headers: {
        "Content-Type": "application/x-www-form-urlencoded",
    },
    transformRequest: [
        (data) => {
            let str = "";
            for (let key in data) {
                str += `${key}=${data[key]}&`;
            }
            return str.replace(/&$/, "");
        },
    ],
});

const http_json = axios.create({
    headers: {
        "Content-Type": "application/json",
    },
    transformRequest: [
        (data) => {
            return JSON.stringify(data);
        },
    ],
});

const http_file = axios.create({
    headers: {
        "Content-Type": "multipart/form-data",
    },
    transformRequest: [
        (data) => {
            const formData = new FormData();
            for (let key in data) {
                formData.append(key, data[key]);
            }
            return formData;
        },
    ],
});

addInterceptors(http_normal);
addInterceptors(http_json);
addInterceptors(http_file);

export default {
    http_normal,
    http_json,
    http_file,
    axios,
};