import { api } from "../..";

export const login = async (postBody) => {
  let response = await api
    .post("auth/login", postBody)
    .then((response) => response.data)
    .catch((e) => e.response.data);
  return response;
};
export const signup = async (postBody) => {
  let response = await api
    .post("auth/register", postBody)
    .then((response) => response.data)
    .catch((e) => e.response.data);
  return response;
};
export const sendLink = async (postBody) => {
  let response = await api
    .post("auth/forgot-password", postBody)
    .then((response) => response.data)
    .catch((e) => e.response.data);
  return response;
};
export const resetPassword = async (postBody, token) => {
  let response = await api
    .post(`auth/reset-password/${token}`, postBody)
    .then((response) => response.data)
    .catch((e) => e.response.data);
  return response;
};
export const changePassword = async (postBody) => {
  let response = await api
    .post("auth/update/password", postBody)
    .then((response) => response.data)
    .catch((e) => e.response.data);
  return response;
};
