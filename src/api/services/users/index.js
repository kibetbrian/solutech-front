import { api } from "../..";

export const getUserDetails = async() => {
    let response = await api
        .get("auth/user")
        .then((response) => response.data)
        .catch((e) => e.response.data);
    return response;
};
export const fetchUsers = async() => {
    let response = await api
        .get("list/all/user")
        .then((response) => response.data)
        .catch((e) => e.response.data);
    return response;
};
export const createUser = async(postBody) => {
    let response = await api
        .post("store/users", postBody)
        .then((response) => response.data)
        .catch((e) => e.response.data);
    return response;
};
export const updateProfile = async(postBody, id) => {
    let response = await api
        .post(`update/user/${id}`, postBody)
        .then((response) => response.data)
        .catch((e) => e.response.data);
    return response;
};