import { api } from "../..";

export const fetchRoles = async () => {
  let response = await api
    .get("roles")
    .then((response) => response.data)
    .catch((e) => e.response.data);
  return response;
};
