import { api } from "../..";

export const fetchStatus = async() => {
    let response = await api
        .get("status")
        .then((response) => response.data)
        .catch((e) => e.response.data);
    return response;
};
export const fetchTasks = async() => {
    let response = await api
        .get("tasks")
        .then((response) => response.data)
        .catch((e) => e.response.data);
    return response;
};
export const fetchAssigns = async() => {
    let response = await api
        .get("user-tasks")
        .then((response) => response.data)
        .catch((e) => e.response.data);
    return response;
};

export const createStatus = async(postBody) => {
    let response = await api
        .post("status", postBody)
        .then((response) => response.data)
        .catch((e) => e.response.data);
    return response;
};
export const creatAssignment = async(postBody) => {
    let response = await api
        .post("user-tasks", postBody)
        .then((response) => response.data)
        .catch((e) => e.response.data);
    return response;
};
export const createTasks = async(postBody) => {
    let response = await api
        .post("tasks", postBody)
        .then((response) => response.data)
        .catch((e) => e.response.data);
    return response;
};

export const deleteUser = async(id) => {
    let response = await api
        .post(`delete/user/${id}`)
        .then((response) => response.data)
        .catch((e) => e.response.data);
    return response;
};

export const deleteStatus = async(id) => {
    let response = await api
        .delete(`status/${id}`)
        .then((response) => response.data)
        .catch((e) => e.response.data);
    return response;
};