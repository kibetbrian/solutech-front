import { Message, MessageBox, Notification } from "element-plus";

const successMsg = (msg) => {
  Message({
    message: msg,
    type: "success",
    customClass: "top-floor",
  });
};

const warnMsg = (msg) => {
  Message({
    message: msg,
    type: "warning",
    customClass: "top-floor",
  });
};

const errorMsg = (msg) => {
  Message({
    message: msg,
    type: "error",
    customClass: "top-floor",
  });
};

const showMsgBox = ({ title, msg, isHTML, type, iconClass }) => {
  return MessageBox.confirm(msg || "", title || "", {
    showClose: false,

    closeOnClickModal: true,

    closeOnPressEscape: true,

    closeOnHashChange: true,

    dangerouslyUseHTMLString: isHTML || false,

    type: type || "info",
    iconClass: iconClass || "",
  });
};

const successTip = ({ title, msg }) => {
  Notification({
    title: title || "",
    message: msg,
    type: "success",
    customClass: "top-floor",
  });
};

const warnTip = ({ title, msg }) => {
  Notification({
    title: title || "",
    message: msg,
    type: "warning",
    customClass: "top-floor",
  });
};

const errorTip = ({ title, msg }) => {
  Notification({
    title: title || "",
    message: msg,
    type: "error",
    customClass: "top-floor",
  });
};

const showTipDiy = ({ title, msg, type, iconClass, isHTML }) => {
  Notification({
    title: title || "",
    message: msg,
    type: type || "info",
    iconClass: iconClass || "",
    dangerouslyUseHTMLString: isHTML || false,
    customClass: "top-floor",
  });
};

export default {
  successMsg,
  warnMsg,
  errorMsg,
  showMsgBox,
  successTip,
  warnTip,
  errorTip,
  showTipDiy,
};
