import { createApp } from "vue";
import App from "./App.vue";
import store from "./store";
import router from "./router";
import "./assets/css/nucleo-icons.css";
import "./assets/css/nucleo-svg.css";
import ArgonDashboard from "./argon-dashboard";
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
import Vue2Editor from "vue2-editor";
import moment from "moment";


const appInstance = createApp(App);

appInstance.config.globalProperties.$moment = moment;
appInstance.use(store);
appInstance.use(router);
appInstance.use(ArgonDashboard);
appInstance.use(ElementPlus);
appInstance.use(Vue2Editor);
appInstance.mount("#app");
