import moment from "moment";

export const formatDate = (value) => {
  return moment(value).format("Do MMM YYYY");
};
export const formatTime = (value) => {
  return moment(value).format("h:mm a");
};
