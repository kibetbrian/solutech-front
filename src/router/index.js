import { createRouter, createWebHistory } from "vue-router";
import Dashboard from "../views/Dashboard.vue";
import Users from "../views/Users.vue";
import Tasks from "../views/Tasks.vue";
import UserDetails from "../views/UserDetails.vue";
import Profile from "../views/Profile.vue";
import Signup from "../views/Signup.vue";
import ForgotPassword from "../views/ForgotPassword.vue";
import Signin from "../views/Signin.vue";
import UserSummary from "../views/UserSummary.vue";
import Status from "../views/Status.vue";
import TaskAssignments from "../views/TaskAssignments.vue";

const routes = [{
        path: "/",
        name: "/",
        redirect: "/signin",
    },
    {
        path: "/signin",
        name: "Signin",
        component: Signin,
    },
    // {
    //     path: "/auth/reset-password/:id",
    //     name: "ResetPassword",
    //     component: ResetPassword,
    // },
    {
        path: "/dashboard-default",
        name: "Dashboard",
        component: Dashboard,
    },
    {
        path: "/users",
        name: "Users",
        component: Users,
    },
    {
        path: "/user/:id/summary",
        name: "PatientSummry",
        component: UserSummary,
    },
    {
        path: "/user/:id",
        name: "UserDetails",
        component: UserDetails,
    },
    {
        path: "/settings/status",
        name: "Status",
        component: Status,
    },
    {
        path: "/tasks",
        name: "Tasks",
        component: Tasks,
    },
    {
        path: "/task-assigns",
        name: "Assignments",
        component: TaskAssignments,
    },
    {
        path: "/profile",
        name: "Profile",
        component: Profile,
    },
    {
        path: "/signup",
        name: "Signup",
        component: Signup,
    },
    {
        path: "/forgot-password",
        name: "ForgotPassword",
        component: ForgotPassword,
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
    linkActiveClass: "active",
});

export default router;